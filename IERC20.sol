// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)

pragma solidity ^0.8.0;

/*
ERC20同质化代币规定了一个同质化代币需要实现的基本接口
*/

interface IERC20{
    /**
    *代币的总发行量
    */
    function totalSupply() external view returns(uint256);

    /**
    *返回一个账户余额
    */
    function balanceOf(address account) external view returns(uint256);

    /**
    *转账（从调用者的代币中移除给receipient账号）
    *会执行Transfer事件
    */
    function transfer(address recipient,uint256 amount) external returns(bool);

    /**
    *返回spender账户被允许使用owner账户的代币数（默认是0）
    *意思是：owner可以授权自己的账户给他人使用自己的代币
    *该值会在approve函数 or transferFrom 函数被调用时发生变化
    */
    function allowance(address owner,address spender) external view returns(uint256);

    /**
    *调用者授予spender指定数量（amount）的代币的使用权，返回bool（要么成功要么失败）
    *会触发Approval事件
    */
    function approve(address spender,uint256 amount) external returns(bool);

    /**
    *授权转账：从sender发送amount数量的代币给到recipient
    *会触发Transfer事件
    */
    function transferFrom(address sender,address recipient,uint256 amount) external returns(bool);

    /**
    *转账事件
    */
    event Transfer(address indexed from,address indexed to,uint256 value);

    /**
    *授权事件
    */
    event Approval(address indexed owner,address indexed spender,uint256 value);
}

