// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)

pragma solidity ^0.8.0;

/**
* 该合约的作用：上下文处理，只是对msg对象做一些包装
*/

abstract contract Context{
    function _msgSender() internal view virtual returns(address){
        return msg.sender;
    }

    function _msgData() internal view virtual returns(bytes calldata){
        return msg.data;
    }
}