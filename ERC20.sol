// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)

pragma solidity ^0.8.0;

import "./Context.sol";
import "./IERC20.sol";
import "./IERC20Metadata.sol";

contract ERC20 is Context,IERC20,IERC20Metadata {

    //总发行量
    uint256 private _totalSupply;

    //代币 name 和 symbol 标识符
    string private _name;
    string private _symbol;

    //构造器
    constructor(string memory name_,string memory symbol_){
        _name = name_;
        _symbol = symbol_;
    }

    //账户 --> 代币数的映射
    mapping(address => uint256) private _balances;

    //账号A --> （账号B --> 数量）的映射：
    //即 账号A对账号B授权指定数量的代币的决定权
    //账号B拥有账号A的指定数量代币的行使权
    mapping(address => mapping(address => uint256)) private _allowances;

    /**
    *查询包括：代币名称查询，代币标志符查询，代币精度查询，代币总发行量查询
    *用户资产查询，授权额度查询
    */

    /**
    *返回指定代币名称
    */
    function name() public view virtual override returns(string memory){
        return _name;
    }

    /**
    *返回代币标识
    */
    function symbol() public view virtual override returns(string memory){
        return _symbol;
    }

    /**
    *返回代币精度
    */
    function decimals() public view virtual override returns(uint8){
        return 18;
    }

    /**
    *代币的总发行量
    */
    function totalSupply() public view virtual override returns(uint256){
        return _totalSupply;
    }

    /**
    *查询账户余额
    */
    function balanceOf(address account) public view virtual override returns(uint256){
        return _balances[account];
    }

    /**
    *获取owner对spender的授权额度
    */
    function allowance(address owner,address spender) public view virtual override returns(uint256){
        return _allowances[owner][spender];
    }
    
    /**
    *转账授权
    *转账触发Transfer事件，更新allowance数量
    *要求：
    *   1.Sender，Recipient不能是零地址
    *   2.Sender拥有不少于amount数量的代币
    *   3.调用者msg.sender不少于amount数量的代币的行使权
    */
    function transferFrom(address sender,address recipient,uint256 amount) public virtual override returns(bool){
        //获取Sender账号对msg.sender账号的授权代币数量
        uint256 currentAllowance = _allowances[sender][_msgSender()];
        //授权数 >= amount 方可以进行转账
        require(currentAllowance >= amount,"ERC20:transfer amount exceeds allowance");
        //执行approve授权函数，更新授权代币的数量
        _approve(sender,_msgSender(),currentAllowance-amount);
        //调用转账函数（）
        _transfer(sender,recipient,amount);
    }

    /**
    *该函数支持将调用者的amount数量的代币授予spender行使
    *调用效果一般是以覆盖的标准
    */

    /**
    *授权额度函数
    */
    function approve(address spender,uint256 amount) public virtual override returns(bool){
        //调用者授予spender指定amount数量的代币行使权
        address owner = _msgSender();
        _approve(owner,spender,amount);
        return true;
    } 

    /**
    *内部函数：真正执行授权额度，将owner的amount数量的代币授予spender行使权
    *事件会触发Approval事件
    */
    function _approve(address owner,address spender,uint256 amount) internal virtual{
        //owner和spender不能是零地址
        require(owner != address(0),"ERC20:approve from the zero address");
        require(spender != address(0),"ERC20:approve to the zero address");

        //授权额度映射，覆盖为amount
        _allowances[owner][spender] = amount;
        emit Approval(owner,spender,amount);
    }

    /**
    *increaseAllowance:在原有的授权额度之上增加额度
    */
    function increaseAllowance(address spender,uint256 addedValue) public virtual returns(bool){
        _approve(_msgSender(),spender,_allowances[_msgSender()][spender] + addedValue);
        return true;
    }

    /**
    *decreaseAllowance:在原有的授权额度之上增加额度
    */
    function decreaseAllowance(address spender,uint256 subtractedValue) public virtual returns(bool){
        //前置判断不能减去负数
        uint256 currentAllowance = _allowances[_msgSender()][spender];
        require(currentAllowance >= subtractedValue,"ERC20:decreased allowance below zero");
        _approve(_msgSender(),spender,currentAllowance - subtractedValue);
        return true;
    }

    /**
    *调用者转amount数量的代币给recipient账号
    *要求：Recipient 不能为零地址（Zero Address），调用者必须拥有少于amount数量的代币
    */
    function transfer(address recipient,uint256 amount) public virtual override returns(bool){
        _transfer(_msgSender(),recipient,amount);
        return true;
    }

    /**
    *转账的内置函数
    *要求：
    *   1.sender不能是零地址
    *   2.receipient不能是零地址（zero address）
    *   3.sender发送方必须拥有大于等于amount数量的代币
    */
    function _transfer(address sender,address recipient,uint256 amount) internal virtual{
        //sender不能是零地址
        require(sender != address(0),"ERC20:transfer from the zero address");
        //recipient不能是零地址
        require(recipient != address(0),"ERC20:transfer to the zero address");
        //空函数，说是Hook钩子函数
        _beforeTokenTransfer(sender,recipient,amount);
        //sender的代币数必须大于amount转账数
        uint256 senderBalance = _balances[sender];
        require(senderBalance >= amount,"ERC20:transfer amount exceeds balance");
        //执行账号代币的转移
        _balances[sender] = senderBalance - amount;
        _balances[recipient] += amount;
        //触发事件
        emit Transfer(sender,recipient,amount);
    }

    function _beforeTokenTransfer(address sender,address recipient,uint256 amount) internal{

    }

    function _afterTokenTransfer(address sender,address recipient,uint256 amount) internal{

    }

    /**
    *内部方法：铸造代币
    */
    function _mint(address account,uint256 amount) public virtual{
        //接收代币地址不能为零地址
        require(account != address(0),"ERC20:mint to the zero address");
        //hook函数
        _beforeTokenTransfer(address(0),account,amount);
        //总发行量叠加，收款账号金额累加
        _totalSupply += amount;
        _balances[account] += amount;
        //触发Transfer事件
        emit Transfer(address(0),account,amount);
        //hook函数
        _afterTokenTransfer(address(0),account,amount);
    }

    /**
    *代币销毁
    */
    function _burn(address account,uint256 amount) internal virtual{
        //接收代币地址不能为零地址
        require(account != address(0),"ERC20:mint to the zero address");
        //hook函数
        _beforeTokenTransfer(address(0),account,amount);

        uint256 accountBalance = _balances[account];
        require(accountBalance >= amount,"ERC20:burn amount exceeds balance");
        _balances[account] = accountBalance - amount;
        _totalSupply -=  amount;

        emit Transfer(account,address(0),amount);
    }
}