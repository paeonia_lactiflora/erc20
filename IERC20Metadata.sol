// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)

pragma solidity ^0.8.0;

/**
*定义了代币的元数据（name,symbol），以及代币的精度
*/

import "./IERC20.sol";

interface IERC20Metadata is IERC20{

    function name() external view returns(string memory);

    function symbol() external view returns(string memory);

    function decimals() external view returns (uint8);
}